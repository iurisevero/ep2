# Exercício Programado 2

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps/eps_2018_2/ep2/wikis/home).
Este projeto foi desenvolvido no Eclipse - IDE

# Pokedex
## Como usar o projeto

* Inicie o programa através da execução do arquivo Pokedex.java
```sh
    O arquivo funciona em conjunto com as outras classes e bibliotecas importadas.
```
* Uma caixa de diálogo irá aparecer informando o inicio da execução.
```sh
    Como informado na caixa, aguarde alguns minutos enquanto o programa carrega
```
* A tela inicial será aberta e dois botões serão apresentados
```sh
    - O botão "Entrar" para usuários já cadastrados;
    - O botão "Registrar" para usuários de primeira viagem
```
* Após efetuado o login, aparecerá a tela de Menu com diversas opções de ação.

## Funcionalidades do Projeto
* Deslogar  (encontrado na barra de tarefas)
```sh
    Volta para tela inicial
```
* Salvar (encontrado na barra de tarefas)
```sh
    Salva os dados do usuário em um arquivo que pode ser encontrado na pasta "Data"
```
* Listar Pokémons  (encontrado na barra de tarefas)
```sh
    Apresenta os pokémons que o usuário possui
```
* Pesquisar Pokémon pelo nome
* Listar Pokémons de um tipo
* Capturar/ Libertar Pokémon
```sh
    Adiciona ou remove um pokémon dos dados do usuário
```

## Bugs e Problemas
* Durante o "loading" existe a possibilidade de dar erro no request da API, sendo um erro de conexão com a internet
```sh
    Esse erro, apesar de aparecer no console, não influencia nos dados captados pelo processo de request.
```
* O JLabel "Usuário" some após o primero login/registro.

## Referências
* Introdução ao Java , Prof. Renato Coral Sampaio
* https://docs.oracle.com/javase/tutorial/uiswing/examples/components/ListDialogRunnerProject/src/components/ListDialog.java
* https://docs.oracle.com/javase/tutorial/uiswing/components/list.html
* https://www.youtube.com/watch?v=p9mZHeWVsAg
* https://www.mkyong.com/java/java-https-client-httpsurlconnection-example/
* https://pokeapi.co/api/v2/
* https://www.pokemon.com/br/pokedex/

