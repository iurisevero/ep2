package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import model.AcountSettings;
import model.Mapas;
import model.Pokemon;
import model.PokemonDataAnalyser;
import model.ThreadParaTipos;
import model.Treinador;
import model.Types;

public class PokedexFrame extends JFrame{
	private JPanel panelTitulo;
	private JPanel panelCadastro;
	private JPanel panelPesquisa;
	private JPanel panelMenu;
	private JLabel pokemonLabel;
	private JTextField pesquisaField;
	private Vector<Pokemon> pokemons;
	private Vector<Types> tipos;
	private Mapas mapa;
	private AcountSettings contas;
	private Treinador treinador;
	
	public PokedexFrame() {
		super("Pokedex");
		ThreadParaTipos thread2 = new ThreadParaTipos();
		thread2.start();
		JOptionPane.showMessageDialog(null, "Carregando... aguarde de 5 a 8 minutos", "Loading", JOptionPane.PLAIN_MESSAGE);
		pokemons = new PokemonDataAnalyser().analisar();
		
		while(thread2.isAlive()) {
			continue;
		}
		tipos = thread2.passarTipos();
		mapa = new Mapas(pokemons, tipos);
		contas = new AcountSettings(mapa);
		treinador = new Treinador("", "", "");
		
		//criarMenu();
		criarFormulario();
	}
	
	private void criarFormulario() {
		setLayout(new BorderLayout());
		
		panelTitulo = new JPanel();
		panelTitulo.setLayout(new FlowLayout());
		
		JLabel titulo = new JLabel("Bem-Vindo ao mundo Pokémon");
		titulo.setFont(new Font("Verdana", Font.PLAIN, 16));
		panelTitulo.add(titulo);		
		
		panelCadastro = new JPanel();
		panelCadastro.setLayout(new FlowLayout());
		
		EntrarAction entrarAction = new EntrarAction();
		JButton entrarButton = new JButton("Entrar");
		entrarButton.addActionListener(entrarAction);
		panelCadastro.add(entrarButton);
		
		RegistrarAction registrarAction = new RegistrarAction();
		JButton registrarButton = new JButton("Registrar");
		registrarButton.addActionListener(registrarAction);
		panelCadastro.add(registrarButton);
		
		JPanel panelFundo = new JPanel();
		panelFundo.setLayout(new FlowLayout());
		pokemonLabel = new JLabel("Pokémon");
		panelFundo.add(pokemonLabel);
		
		add(panelTitulo, BorderLayout.NORTH);
		add(panelCadastro, BorderLayout.CENTER);
		add(panelFundo, BorderLayout.SOUTH);
	}
	
	private class EntrarAction implements ActionListener{

		public void actionPerformed(ActionEvent event) {
			JPanel panelLogin = new JPanel();
			panelLogin.setLayout(new FlowLayout());
			JLabel usuarioLabel = new JLabel("Usuário");
			JTextField usuarioField = new JTextField(40);
			JLabel senhaLabel = new JLabel("Senha");
			JTextField senhaField = new JTextField(40);
			panelLogin.add(usuarioLabel);
			panelLogin.add(usuarioField);
			panelLogin.add(senhaLabel);
			panelLogin.add(senhaField);
			panelLogin.setVisible(true);
			class CancelarAction implements ActionListener{
				public void actionPerformed(ActionEvent event) {
					panelLogin.setVisible(false);
					pokemonLabel.setText("Pokémon");
					panelCadastro.setVisible(true);
				}		
			}
			CancelarAction cancelarAction = new CancelarAction();
			
			class ConfirmarAction implements ActionListener{
				public void actionPerformed(ActionEvent event) {	
					String login = usuarioField.getText();
					String senha = senhaField.getText();
					treinador = contas.logarConta(login, senha);
					if(treinador.getNome().intern()== "LOGIN_ERROR"){
						JOptionPane.showMessageDialog(null, "Usuário não encontrado", "Login", JOptionPane.ERROR_MESSAGE);
						usuarioField.setText("");
						senhaField.setText("");
					}
					else if(treinador.getNome().intern()== "SENHA_ERROR") {
						JOptionPane.showMessageDialog(null, "Senha inválida", "Login", JOptionPane.ERROR_MESSAGE);
						usuarioField.setText("");
						senhaField.setText("");
					}
					else if(treinador.getNome().intern()== ""){
						JOptionPane.showMessageDialog(null, "ERRO", "Login", JOptionPane.ERROR_MESSAGE);
						usuarioField.setText("");
						senhaField.setText("");
					}
					else {
						JOptionPane.showMessageDialog(null, "Usuário encontrado! Entrando...", "Login", JOptionPane.PLAIN_MESSAGE);
						panelLogin.setVisible(false);
						panelTitulo.setVisible(false);
						pokemonLabel.setVisible(false);
						criarPaginaPrincipal();
					}
				}		
			}
			ConfirmarAction confirmarAction = new ConfirmarAction();
			
			JButton confirmarButton = new JButton("Confirmar");
			confirmarButton.addActionListener(confirmarAction);
			
			JButton cancelarButton = new JButton("Cancelar");
			cancelarButton.addActionListener(cancelarAction);
			
			panelLogin.add(confirmarButton);
			panelLogin.add(cancelarButton);
			
			panelCadastro.setVisible(false);
			add(panelLogin, BorderLayout.CENTER);
			pokemonLabel.setText("Entrando");
		}
		
	}
	
	private class RegistrarAction implements ActionListener{

		public void actionPerformed(ActionEvent event) {
			JPanel panelRegister = new JPanel();
			panelRegister.setLayout(new FlowLayout());
			JLabel usuarioLabel = new JLabel("Usuário");
			JTextField usuarioField = new JTextField(40);
			JLabel senhaLabel = new JLabel("Senha");
			JTextField senhaField = new JTextField(40);
			JLabel nomeLabel = new JLabel("Nome");
			JTextField nomeField = new JTextField(40);
			panelRegister.add(usuarioLabel);
			panelRegister.add(usuarioField);
			panelRegister.add(senhaLabel);
			panelRegister.add(senhaField);
			panelRegister.add(nomeLabel);
			panelRegister.add(nomeField);
			panelRegister.setVisible(true);
			class CancelarAction implements ActionListener{
				public void actionPerformed(ActionEvent event) {
					panelRegister.setVisible(false);
					pokemonLabel.setText("Pokémon");
					panelCadastro.setVisible(true);
				}		
			}
			CancelarAction cancelarAction = new CancelarAction();
			
			class ConfirmarAction implements ActionListener{
				public void actionPerformed(ActionEvent event) {	
					String login = usuarioField.getText();
					String senha = senhaField.getText();
					String nome = nomeField.getText();
					treinador = contas.criarConta(login, senha, nome);
					if(treinador.getNome().intern()== "LOGIN_ERROR"){
						JOptionPane.showMessageDialog(null, "Nome de usuário já utilizado", "Register", JOptionPane.ERROR_MESSAGE);
						usuarioField.setText("");
						senhaField.setText("");
						nomeField.setText("");
					}
					else if(treinador.getNome().intern()== ""){
						JOptionPane.showMessageDialog(null, "ERRO", "Register", JOptionPane.ERROR_MESSAGE);
						usuarioField.setText("");
						senhaField.setText("");
						nomeField.setText("");
					}
					else {
						JOptionPane.showMessageDialog(null, "Usuário criado! Entrando...", "Register", JOptionPane.PLAIN_MESSAGE);
						panelRegister.setVisible(false);
						panelTitulo.setVisible(false);
						pokemonLabel.setVisible(false);
						
						criarPaginaPrincipal();
					}	
				}		
			}
			ConfirmarAction confirmarAction = new ConfirmarAction();
			
			JButton confirmarButton = new JButton("Confirmar");
			confirmarButton.addActionListener(confirmarAction);
			
			JButton cancelarButton = new JButton("Cancelar");
			cancelarButton.addActionListener(cancelarAction);
			
			panelRegister.add(confirmarButton);
			panelRegister.add(cancelarButton);
			
			panelCadastro.setVisible(false);
			add(panelRegister, BorderLayout.CENTER);
			pokemonLabel.setText("Registrando");
		}
		
	}
	
	private void criarPaginaPrincipal() {
		JMenu menuUsuario = new JMenu(treinador.getNome());
		panelMenu = new JPanel();
		panelPesquisa = new JPanel();
		panelPesquisa.setLayout(new FlowLayout());
		JLabel pesquisaLabel = new JLabel("");
		pesquisaField = new JTextField(30);
		panelPesquisa.add(pesquisaLabel);
		panelPesquisa.add(pesquisaField);
		panelPesquisa.setVisible(false);
		
		panelMenu.setLayout(new FlowLayout());
		JMenuBar barra = new JMenuBar();
		setJMenuBar(barra);
		
		class DeslogarAction implements ActionListener{

			public void actionPerformed(ActionEvent arg0) {
				treinador.salvar();
				barra.setVisible(false);
				panelMenu.setVisible(false);
				panelTitulo.setVisible(true);
				panelCadastro.setVisible(true);
				pokemonLabel.setText("Pokémon");
				pokemonLabel.setVisible(true);
			}
			
		}
		DeslogarAction deslogarAction = new DeslogarAction();
		
		JMenuItem menuItemDeslogar = new JMenuItem("Deslogar");
		menuItemDeslogar.addActionListener(deslogarAction);
		menuUsuario.add(menuItemDeslogar);
		
		class SalvarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				treinador.salvar();
			}
			
		}
		SalvarAction salvarAction = new SalvarAction();
		
		JMenuItem menuItemSalvar = new JMenuItem("Salvar");
		menuItemSalvar.addActionListener(salvarAction);
		menuUsuario.add(menuItemSalvar);
		
		class ListarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				JFrame meusPokemonsFrame = new JFrame("Meus Pokémons");		
				meusPokemonsFrame.setSize(530, 260); 
				meusPokemonsFrame.setVisible(true);
				meusPokemonsFrame.setLayout(new BorderLayout());
				JPanel panelTopo = new JPanel();
				panelTopo.setLayout(new FlowLayout());
				JLabel nomeUsuarioLabel = new JLabel(treinador.getNome().toUpperCase());
				nomeUsuarioLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
				panelTopo.add(nomeUsuarioLabel);	
				meusPokemonsFrame.add(panelTopo, BorderLayout.NORTH);
				
				Vector<Pokemon> meusPokemons = treinador.getPokemons();
				int pokemonsSize = meusPokemons.size();
				JList listaPokemons;
				DefaultListModel modeloPoke = new DefaultListModel();
				
				for(int i=0; i<pokemonsSize; ++i) {
					modeloPoke.addElement(meusPokemons.get(i).getNome());
				}
				listaPokemons = new JList(modeloPoke);
				listaPokemons.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
				listaPokemons.setLayoutOrientation(JList.VERTICAL);
				listaPokemons.setVisibleRowCount(5);
				JScrollPane listScroller1 = new JScrollPane(listaPokemons);
				listScroller1.setPreferredSize(new Dimension(250, 80));
				JLabel labelPokemons = new JLabel("Pokemons");
		        labelPokemons.setLabelFor(listaPokemons);
				JPanel panel = new JPanel();
				panel.setLayout(new FlowLayout());
				panel.add(labelPokemons);
				panel.add(Box.createRigidArea(new Dimension(0,5)));
				panel.add(listScroller1);
				panel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
				meusPokemonsFrame.add(panel, BorderLayout.CENTER);
			}
			
		}
		ListarAction listarAction = new ListarAction();
		JMenuItem menuItemPokemons = new JMenuItem("Listar Pokémons");
		menuItemPokemons.addActionListener(listarAction);
		menuUsuario.add(menuItemPokemons);
		
		class PesquisarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				pesquisaLabel.setText("Nome do Pokémon");
				panelPesquisa.setVisible(true);
				panelMenu.setVisible(false);
				criarPesquisaPokemon();
			}
			
		}
		PesquisarAction pesquisarAction = new PesquisarAction();
		JButton pesquisarPokemonButton = new JButton("Pesquisar Pokémon");
		pesquisarPokemonButton.addActionListener(pesquisarAction);
		
		class PesquisarTipoAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				pesquisaLabel.setText("Nome do Tipo");
				panelPesquisa.setVisible(true);
				panelMenu.setVisible(false);
				criarPesquisaTipo();
			}
			
		}
		PesquisarTipoAction pesquisarTipoAction = new PesquisarTipoAction();
		JButton listarTipoButton = new JButton("Pesquisar Tipos");
		listarTipoButton.addActionListener(pesquisarTipoAction);
		
		class CapturarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				pesquisaLabel.setText("Nome do Pokémon");
				panelPesquisa.setVisible(true);
				panelMenu.setVisible(false);
				criarCapturaPokemon();
			}
			
		}
		CapturarAction capturarAction = new CapturarAction();
		JButton capturarButton = new JButton("Capturar Pokémon");
		capturarButton.addActionListener(capturarAction);
		
		class LibertarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				pesquisaLabel.setText("Nome do Pokémon");
				panelPesquisa.setVisible(true);
				panelMenu.setVisible(false);
				criarLibertarPokemon();
			}
			
		}
		LibertarAction libertarAction = new LibertarAction();
		JButton libertarButton = new JButton("Libertar Pokémon");
		libertarButton.addActionListener(libertarAction);
		
		panelMenu.add(pesquisarPokemonButton);
		panelMenu.add(listarTipoButton);
		panelMenu.add(capturarButton);
		panelMenu.add(libertarButton);
		
		barra.add(menuUsuario);
		add(panelPesquisa, BorderLayout.NORTH);
		add(panelMenu, BorderLayout.CENTER);
	}

	public void criarPesquisaPokemon() {
		JPanel panelParaPesquisa = new JPanel();
		panelParaPesquisa.setLayout(new FlowLayout());
		
		class PesquisarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				String nome = pesquisaField.getText();
				if(mapa.encontraPokemon(nome).getNome().intern()!="") {
					Pokemon pokemon = mapa.encontraPokemon(nome);
					JFrame pokemonData = new JFrame(nome);
					
					pokemonData.setSize(700, 260); 
					pokemonData.setVisible(true);
					pokemonData.setLayout(new BorderLayout());
					JPanel panelTopo = new JPanel();
					panelTopo.setLayout(new FlowLayout());
					JLabel nomePokemonLabel = new JLabel(nome.toUpperCase());
					nomePokemonLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
					panelTopo.add(nomePokemonLabel);
					pokemonData.add(panelTopo, BorderLayout.NORTH);
					JLabel alturaLabel = new JLabel("Altura " + pokemon.getHeight());
					JLabel pesoLabel = new JLabel("Peso " + pokemon.getWeight());
					int abilitiesSize = pokemon.getAbilities().size();
					JLabel abilitiesLabel[] = new JLabel[abilitiesSize+1];
					abilitiesLabel[0] = new JLabel("Habilidades");
					for(int i=1; i<abilitiesSize+1; ++i) {
						abilitiesLabel[i] = new JLabel(pokemon.getAbilities().get(i-1));
					}
					int tiposSize = pokemon.getType().size();
					JLabel tiposLabel[] = new JLabel[tiposSize+1];
					tiposLabel[0] = new JLabel("Tipo(s)");
					for(int i=1; i<tiposSize+1; ++i) {
						tiposLabel[i] = new JLabel(pokemon.getType().get(i-1));
					}
					JPanel panelOeste = new JPanel();
					panelOeste.setLayout(new FlowLayout());
					panelOeste.add(alturaLabel);
					panelOeste.add(pesoLabel);
					
					JPanel panelCentral = new JPanel();
					panelCentral.setLayout(new FlowLayout());
					for(int i=0; i<abilitiesSize+1; ++i) {
						panelCentral.add(abilitiesLabel[i]);
					}
					
					JPanel panelLeste = new JPanel();
					panelLeste.setLayout(new FlowLayout());
					for(int i=0; i<tiposSize+1; ++i) {
						panelLeste.add(tiposLabel[i]);
					}
					pokemonData.add(panelCentral, BorderLayout.CENTER);
					pokemonData.add(panelLeste, BorderLayout.EAST);
					pokemonData.add(panelOeste, BorderLayout.WEST);
				}
				else {
					JOptionPane.showMessageDialog(null, "Pokémon não encontrado", "Pesquisa", JOptionPane.ERROR_MESSAGE);
				}
				pesquisaField.setText("");
			}
			
		}
		PesquisarAction pesquisarAction = new PesquisarAction();
		JButton pesquisarButton = new JButton("Pesquisar");
		pesquisarButton.addActionListener(pesquisarAction);
		
		class FinalizarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				pesquisaField.setText("");
				panelParaPesquisa.setVisible(false);
				panelPesquisa.setVisible(false);
				panelMenu.setVisible(true);
			}
			
		}
		FinalizarAction finalizarAction = new FinalizarAction();
		JButton finalizarButton = new JButton("Finalizar pesquisa");
		finalizarButton.addActionListener(finalizarAction);
		
		panelParaPesquisa.add(pesquisarButton);
		panelParaPesquisa.add(finalizarButton);
		
		add(panelParaPesquisa, BorderLayout.CENTER);
	}
	
	public void criarPesquisaTipo() {
		JPanel panelParaPesquisa = new JPanel();
		panelParaPesquisa.setLayout(new FlowLayout());
		
		class PesquisarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				String nome = pesquisaField.getText();
				if(mapa.encontraTipo(nome).getNome().intern()!="") {
					Types tipo = mapa.encontraTipo(nome);
					JFrame tipoData = new JFrame(nome);		
					tipoData.setSize(1200, 260); 
					tipoData.setVisible(true);
					tipoData.setLayout(new BorderLayout());
					JPanel panelTopo = new JPanel();
					panelTopo.setLayout(new FlowLayout());
					JLabel nomePokemonLabel = new JLabel(nome.toUpperCase());
					nomePokemonLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
					panelTopo.add(nomePokemonLabel);	
					tipoData.add(panelTopo, BorderLayout.NORTH);
					
					Vector<String> forte = tipo.getForteContra();
					int forteSize = forte.size();
					JList listaForte;
					DefaultListModel modeloForte = new DefaultListModel();
					
					for(int i=0; i<forteSize; ++i) {
						modeloForte.addElement(forte.get(i));
					}
					listaForte = new JList(modeloForte);
					listaForte.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
					listaForte.setLayoutOrientation(JList.VERTICAL);
					listaForte.setVisibleRowCount(5);
					JScrollPane listScroller1 = new JScrollPane(listaForte);
					listScroller1.setPreferredSize(new Dimension(250, 80));
					JLabel labelForte = new JLabel("Forte Contra");
			        labelForte.setLabelFor(listaForte);
					JPanel panelOeste = new JPanel();
					panelOeste.setLayout(new FlowLayout());
					panelOeste.add(labelForte);
					panelOeste.add(Box.createRigidArea(new Dimension(0,5)));
					panelOeste.add(listScroller1);
					panelOeste.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
					tipoData.add(panelOeste, BorderLayout.WEST);
					
					
					Vector<String> fraco = tipo.getFracoContra();
					int fracoSize = fraco.size();
					JList listaFraco;
					DefaultListModel modeloFraco = new DefaultListModel();
					
					for(int i=0; i<fracoSize; ++i) {
						modeloFraco.addElement(fraco.get(i));
					}
					listaFraco = new JList(modeloFraco);
					listaFraco.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
					listaFraco.setLayoutOrientation(JList.VERTICAL);
					listaFraco.setVisibleRowCount(5);
					JScrollPane listScroller2 = new JScrollPane(listaFraco);
					listScroller2.setPreferredSize(new Dimension(250, 80));
					JLabel labelFraco = new JLabel("Fraco Contra");
			        labelFraco.setLabelFor(listaFraco);
					JPanel panelLeste = new JPanel();
					panelLeste.setLayout(new FlowLayout());
					panelLeste.add(labelFraco);
					panelLeste.add(Box.createRigidArea(new Dimension(0,5)));
					panelLeste.add(listScroller2);
					panelLeste.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
					tipoData.add(panelLeste, BorderLayout.EAST);
					
					Vector<String> pokemonsDoTipo = tipo.getPokemonsDoTipo();
					int pokemonsDoTipoSize = pokemonsDoTipo.size();
					JList listaPokemons;
					DefaultListModel modeloPoke = new DefaultListModel();
					
					for(int i=0; i<pokemonsDoTipoSize; ++i) {
						modeloPoke.addElement(pokemonsDoTipo.get(i));
					}
					listaPokemons = new JList(modeloPoke);
					listaPokemons.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
					listaPokemons.setLayoutOrientation(JList.VERTICAL);
					listaPokemons.setVisibleRowCount(5);
					JScrollPane listScroller3 = new JScrollPane(listaPokemons);
					listScroller3.setPreferredSize(new Dimension(250, 80));
					JLabel labelPoke = new JLabel("Pokémons");
			        labelPoke.setLabelFor(listaPokemons);
			        
					JPanel panelCentro = new JPanel();
					panelCentro.setLayout(new FlowLayout());
					panelCentro.add(labelPoke);
					panelCentro.add(Box.createRigidArea(new Dimension(0,5)));
					panelCentro.add(listScroller3);
					panelCentro.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
					tipoData.add(panelCentro, BorderLayout.CENTER);
					
				}
				else {
					JOptionPane.showMessageDialog(null, "Tipo não encontrado", "Pesquisa", JOptionPane.ERROR_MESSAGE);
				}
				pesquisaField.setText("");
			}
			
		}
		PesquisarAction pesquisarAction = new PesquisarAction();
		JButton pesquisarButton = new JButton("Pesquisar");
		pesquisarButton.addActionListener(pesquisarAction);
		
		class FinalizarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				pesquisaField.setText("");
				panelParaPesquisa.setVisible(false);
				panelPesquisa.setVisible(false);
				panelMenu.setVisible(true);
			}
			
		}
		FinalizarAction finalizarAction = new FinalizarAction();
		JButton finalizarButton = new JButton("Finalizar pesquisa");
		finalizarButton.addActionListener(finalizarAction);
		
		panelParaPesquisa.add(pesquisarButton);
		panelParaPesquisa.add(finalizarButton);
		
		add(panelParaPesquisa, BorderLayout.CENTER);
	}
	
	public void criarCapturaPokemon() {
		JPanel panelParaCaptura = new JPanel();
		panelParaCaptura.setLayout(new FlowLayout());
		
		class CapturarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				String nome = pesquisaField.getText();
				if(mapa.encontraPokemon(nome).getNome().intern()!="") {
					treinador.add(mapa.encontraPokemon(nome));
				}
				else {
					JOptionPane.showMessageDialog(null, "Pokémon não encontrado", "Captura", JOptionPane.ERROR_MESSAGE);
				}
				pesquisaField.setText("");
			}
			
		}
		CapturarAction capturarAction = new CapturarAction();
		JButton capturarButton = new JButton("Capturar");
		capturarButton.addActionListener(capturarAction);
		
		class FinalizarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				pesquisaField.setText("");
				panelParaCaptura.setVisible(false);
				panelPesquisa.setVisible(false);
				panelMenu.setVisible(true);
			}
			
		}
		FinalizarAction finalizarAction = new FinalizarAction();
		JButton finalizarButton = new JButton("Finalizar captura");
		finalizarButton.addActionListener(finalizarAction);
		
		panelParaCaptura.add(capturarButton);
		panelParaCaptura.add(finalizarButton);
		
		add(panelParaCaptura, BorderLayout.CENTER);
	}
	
	public void criarLibertarPokemon() {
		JPanel panelParaLibertar = new JPanel();
		panelParaLibertar.setLayout(new FlowLayout());
		
		class LibertarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				String nome = pesquisaField.getText();
				if(mapa.encontraPokemon(nome).getNome().intern()!="") {
					treinador.remove(mapa.encontraPokemon(nome));
				}
				else {
					JOptionPane.showMessageDialog(null, "Pokémon não encontrado", "Libertar", JOptionPane.ERROR_MESSAGE);
				}
				pesquisaField.setText("");
			}
			
		}
		LibertarAction libertarAction = new LibertarAction();
		JButton libertarButton = new JButton("Libertar");
		libertarButton.addActionListener(libertarAction);
		
		class FinalizarAction implements ActionListener{
			
			public void actionPerformed(ActionEvent arg0) {
				pesquisaField.setText("");
				panelParaLibertar.setVisible(false);
				panelPesquisa.setVisible(false);
				panelMenu.setVisible(true);
			}
			
		}
		FinalizarAction finalizarAction = new FinalizarAction();
		JButton finalizarButton = new JButton("Finalizar libertar");
		finalizarButton.addActionListener(finalizarAction);
		
		panelParaLibertar.add(libertarButton);
		panelParaLibertar.add(finalizarButton);
		
		add(panelParaLibertar, BorderLayout.CENTER);
	}
}
