package model;

import java.util.Vector;

public class Types {
	private String nome;
	private Vector<String> forteContra;
	private Vector<String> fracoContra;
	private Vector<String> pokemonsDoTipo;
	
	public Types() {
		nome = "";
		forteContra = new Vector<String>();
		fracoContra = new Vector<String>();
		pokemonsDoTipo = new Vector<String>();
	}
	public Types(String nome) {
		setNome(nome);
		forteContra = new Vector<String>();
		fracoContra = new Vector<String>();
		pokemonsDoTipo = new Vector<String>();
	}
	
	public Types(String nome, Vector<String> fracoContra, Vector<String> forteContra, Vector<String> pokemonsDoTipo) {
		setNome(nome);
		setFracoContra(fracoContra);
		setForteContra(forteContra);	
		setPokemonsDoTipo(pokemonsDoTipo);
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Vector<String> getForteContra() {
		return forteContra;
	}
	public void setForteContra(Vector<String> forteContra) {
		this.forteContra = forteContra;
	}
	public Vector<String> getFracoContra() {
		return fracoContra;
	}
	public void setFracoContra(Vector<String> fracoContra) {
		this.fracoContra = fracoContra;
	}
	public Vector<String> getPokemonsDoTipo() {
		return pokemonsDoTipo;
	}
	public void setPokemonsDoTipo(Vector<String> pokemonsDoTipo) {
		this.pokemonsDoTipo = pokemonsDoTipo;
	}
	
	public void imprimeDados() {
		System.out.println("     Tipo: " + getNome());
		System.out.println("     Fraco Contra:");
		for(String fraqueza: fracoContra) {
			System.out.print("     " + fraqueza + "\t");
		}
		System.out.println();
		System.out.println("     Forte Contra:");
		for(String forca: forteContra) {
			System.out.print("     " + forca + "\t");
		}
		System.out.println("\n");
	}
	
	public void visualizarPokemons() {
		System.out.println("Pokemons do tipo " + nome + ":");
		int i=0;
		for(String pokemon: pokemonsDoTipo) {
			System.out.printf("     %-20s     ",pokemon);
			if(i%5==4) {
				System.out.println();
			}
			i++;
		}
	}
	
 }
