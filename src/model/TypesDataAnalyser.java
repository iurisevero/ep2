package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

import javax.net.ssl.HttpsURLConnection;

public class TypesDataAnalyser {

	public Vector<Types> analisar(){

		String httpsUrl = "https://pokeapi.co/api/v2/type/";
		URL url;
		try {
	    	
			url = new URL(httpsUrl);
		    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();

		    Vector<String> apiTypes = apiLinkScanner(con);
		    Vector<String> typeNomes = new Vector<String>();
		    Vector<String> typeLinks = new Vector<String>();		
		    for(String content: apiTypes) {
			    if(content.contains("url")) {
			    	int begin = content.lastIndexOf(": \"");
			    	int end = content.indexOf("/\"");
			    	typeLinks.add(content.substring(begin+3, end+1));
		    	}
			    else {
			    	int begin = content.lastIndexOf(": \"");
			    	int end = content.indexOf("\",");
			    	typeNomes.add(content.substring(begin+3, end));
			    }
		    }
		    Vector<Types> tipos = typeLinkScanner(typeNomes, typeLinks);
		    return tipos;
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
	    } 
	    catch (IOException e) {
	    	e.printStackTrace();
	    }
		return null;
	}

	private Vector<String> apiLinkScanner(HttpsURLConnection con){
		if(con!=null){
				
			try {
						
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String input;
				Vector<String> dados = new Vector<String>();
			   
				while((input = br.readLine()) != null) {
					
					if(input.contains("url")||input.contains("name")) {
						dados.add(input);
					}
				}
				
				br.close();
				return dados;	
			} 
			catch (IOException e) {
				e.printStackTrace();
			}		
		}
		return null;
	}
	
	private Vector<Types> typeLinkScanner(Vector<String> typeNomes, Vector<String> typeLinks) {
		
		if(typeNomes.size()!=0) {
			Vector<Types> tipos = new Vector<Types>();
			int size = typeNomes.size();
			for(int i=0; i<size; ++i){
				String nome = typeNomes.get(i);
				Vector<String> forteContra = new Vector<String>();
				Vector<String> fracoContra = new Vector<String>();
				Vector<String> pokemonsDoTipo = new Vector<String>();
				String httpsUrl = typeLinks.get(i);
				URL url;
				try {
			    	
					url = new URL(httpsUrl);
				    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
				   
				    if(con!=null){						
						try {
									
							BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
							String input;
							
							while((input = br.readLine()) != null) {	
								if(input.contains("double_damage_from")) {
									do {
										input = br.readLine();
										if(input.contains("name")) {
											int begin = input.indexOf(": \"");
											int end = input.indexOf("\",");
											if(begin>=0&&end>=0) {
												fracoContra.add(input.substring(begin+3, end));
											}
										}
									}while(!input.contains("],"));
								}
								if(input.contains("double_damage_to")) {
									do {
										input = br.readLine();
										if(input.contains("name")) {
											int begin = input.indexOf(": \"");
											int end = input.indexOf("\",");
											if(begin>=0&&end>=0) {
												forteContra.add(input.substring(begin+3, end));
											}
										}
									}while(!input.contains("],"));
								}
								
								if(input.contains("pokemon")) {
									while(!input.contains("]")){
										input = br.readLine();
										if(input.contains("name")) {
											int begin = input.indexOf(": \"");
											int end = input.indexOf("\",");
											if(begin>=0&&end>=0) {
												pokemonsDoTipo.add(input.substring(begin+3, end));
											}
										}
									}
								}
							}
							br.close();					
						} 
						catch (IOException e) {
							e.printStackTrace();
						}		
					}
				    else {
				    	return null;
				    }
				}
				catch (MalformedURLException e) {
					e.printStackTrace();
			    } 
			    catch (IOException e) {
			    	e.printStackTrace();
			    }
				
				Types tipoAnalisado = new Types(nome, fracoContra, forteContra, pokemonsDoTipo);
				tipos.add(tipoAnalisado);
			}
			return tipos;
		}   
		return null;
	}

}

