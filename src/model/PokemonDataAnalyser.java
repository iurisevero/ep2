package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Vector;

import javax.net.ssl.HttpsURLConnection;

public class PokemonDataAnalyser {
	public Vector<Pokemon> analisar(){

		String httpsUrl = "https://pokeapi.co/api/v2/pokemon/";
		URL url;
		try {
	    	
			url = new URL(httpsUrl);
		    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();

		    Vector<String> apiPokemons = apiLinkScanner(con);
		    Vector<String> pokemonNomes = new Vector<String>();
		    Vector<String> pokemonLinks = new Vector<String>();		
		    for(String content: apiPokemons) {
			    if(content.contains("url")) {
			    	int begin = content.lastIndexOf(": \"");
			    	int end = content.indexOf("/\"");
			    	pokemonLinks.add(content.substring(begin+3, end+1));
		    	}
			    else {
			    	int begin = content.lastIndexOf(": \"");
			    	int end = content.indexOf("\",");
			    	pokemonNomes.add(content.substring(begin+3, end));
			    }
		    }
		    
		    Vector<Pokemon> pokemons = pokemonLinkScanner(pokemonNomes, pokemonLinks);		    
		    return pokemons;
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
	    } 
	    catch (IOException e) {
	    	e.printStackTrace();
	    }
		return null;
	}

	private Vector<String> apiLinkScanner(HttpsURLConnection con){
		if(con!=null){
				
			try {
						
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String input;
				Vector<String> dados = new Vector<String>();
			   
				while((input = br.readLine()) != null) {
					
					if(input.contains("url")||input.contains("name")) {
						dados.add(input);
					}
				}
				
				br.close();
				return dados;	
			} 
			catch (IOException e) {
				e.printStackTrace();
			}		
		}
		return null;
	}
	
	private Vector<Pokemon> pokemonLinkScanner(Vector<String> pokemonNomes, Vector<String> pokemonLinks) {
		
		if(pokemonNomes.size()!=0) {
			Vector<Pokemon> pokemons = new Vector<Pokemon>();
			int size = pokemonNomes.size();
			ThreadParaPokemons thread[] = new ThreadParaPokemons[size+10]; 
			for(int j=0; j<151; ++j) {
				thread[j] = new ThreadParaPokemons(pokemonNomes.get(j), pokemonLinks.get(j));
				thread[j].start();
			}
			for(int i=151; i<size;){
				if(!(thread[i%151].isAlive())) {	
					pokemons.add(thread[i%151].getPokemon());
					thread[i%151] = new ThreadParaPokemons(pokemonNomes.get(i), pokemonLinks.get(i));
					thread[i%151].start();
					i++;
					try {
						new Thread().sleep(150);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}	
				}
			}
			for(int i=0; i<151;) {
				if(!thread[i].isAlive()) {
					pokemons.add(thread[i].getPokemon());
					i++;
				}
			}
			return pokemons;
		}   
		return null;
	}
}