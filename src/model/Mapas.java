package model;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class Mapas {
	private Map<String, Pokemon> pokemonsMapeados = new HashMap<String, Pokemon>();
	private Map<String, Types> tiposMapeados = new HashMap<String, Types>();
	
	public Mapas(Vector<Pokemon> pokemons, Vector<Types> types){
		for(Pokemon p: pokemons){
			pokemonsMapeados.put(p.getNome(), p);
		}
		
		for(Types t: types){
			tiposMapeados.put(t.getNome(), t);
		}
	}
	
	public Vector<Pokemon> mapeiaPokemon(Vector<String> pokemonsArquivados){
		Vector<Pokemon> pokemonsLidos = new Vector<Pokemon>();
		for(String s: pokemonsArquivados) {
			pokemonsLidos.add(pokemonsMapeados.get(s));
		}
		return pokemonsLidos;
	}
	
	public Pokemon encontraPokemon(String nome) {
		if(pokemonsMapeados.containsKey(nome)) {
			return pokemonsMapeados.get(nome);
		}
		else {
			return new Pokemon();
		}
	}
	
	public Vector<Types> mapeiaTipo(Vector<String> tiposArquivados){
		Vector<Types> tiposLidos = new Vector<Types>();
		for(String s: tiposArquivados) {
			tiposLidos.add(tiposMapeados.get(s));
		}
		return tiposLidos;
	}
	
	public Types encontraTipo(String nome) {
		if(tiposMapeados.containsKey(nome)) {
			return tiposMapeados.get(nome);
		}
		else {
			return new Types();
		}
	}	
}
