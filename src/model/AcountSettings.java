package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.JOptionPane;

public class AcountSettings {
	private Mapas mapa;
	
	public AcountSettings(Mapas mapa) {
		setMaps(mapa);
	}
	
	public void setMaps(Mapas mapa) {
		this.mapa = mapa;
	}

	public Treinador criarConta(String login, String senha, String nome) {

		while(true) {
			try {
				FileReader arq = new FileReader("data/" + login + ".txt");
				arq.close();
				return new Treinador("LOGIN_ERROR", "", "");				
			} catch (FileNotFoundException e) {
				break;
			} catch (IOException e) {
				e.printStackTrace();
			}
					
		}
		return new Treinador(nome, login, senha);
	}
	
	public Treinador logarConta(String login, String senha) {
		try {
			FileReader arq = new FileReader("data/" + login + ".txt");
			BufferedReader lerArq = new BufferedReader(arq);
			try {
				String linha  = lerArq.readLine();
				if(linha.intern() == login.intern()) {
					linha = lerArq.readLine();	
					if(linha.intern() == senha.intern()) {
						linha = lerArq.readLine();
						String nome = linha;
						Vector<String> pokemonsArquivados = new Vector<String>();
						while((linha = lerArq.readLine()) != null) {
							pokemonsArquivados.add(linha);
						}
						Vector<Pokemon> pokemonsLidos = mapa.mapeiaPokemon(pokemonsArquivados);
						arq.close();
						Treinador treinadorCadastrado = new Treinador(nome, login, senha, pokemonsLidos);
						return treinadorCadastrado;
					}
					else {
						arq.close();
						return new Treinador("SENHA_ERROR", "SENHA_ERROR", "");
					}
					
				}	
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			return new Treinador("LOGIN_ERROR", "", "");
		}
		return new Treinador("", "", "");
	}
	
}
