package model;

import java.util.Vector;

public class ThreadParaTipos extends Thread {
	private Vector<Types> tipos;
	public void run() {
		tipos = new TypesDataAnalyser().analisar();
	}
	
	public Vector<Types> passarTipos() {
		return tipos;
	}
}
