package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

import javax.net.ssl.HttpsURLConnection;

public class ThreadParaPokemons extends Thread {
	private String nome;
	private String link;
	private Pokemon pokemon;

	public ThreadParaPokemons(String nome, String link) {
		setNome(nome);
		setLink(link);
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public Pokemon getPokemon() {
		return pokemon;
	}
	public void setPokemon(Pokemon pokemon) {
		this.pokemon = pokemon;
	}
	
	public void run() {
		Integer height=0, weight=0;
		String nome = getNome();
		Vector<String> abilities = new Vector<String>();
		Vector<String> type = new Vector<String>();
		String httpsUrl = getLink();
		URL url;
		try {
	    	
			url = new URL(httpsUrl);
		    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
		   
		    if(con!=null){						
				try {
							
					BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
					String input;					
					while((input = br.readLine()) != null) {	
						if(input.contains("\"ability\"")) {
							input = br.readLine();
							int begin = input.indexOf(": \"");
							int end = input.indexOf("\",");
							abilities.add(input.substring(begin+3, end));
						}
						
						if(input.contains("height")) {
							int begin = input.indexOf(": ");
							int end = input.indexOf(",");
							height = Integer.parseInt(input.substring(begin+2, end));									
						}
						
						if(input.contains("weight")) {
							int begin = input.indexOf(": ");
							int end = input.length();
							weight = Integer.parseInt(input.substring(begin+2, end));									
						}
						
						if(input.contains("\"type\"")) {
							input = br.readLine();
							int begin = input.indexOf(": \"");
							int end = input.indexOf("\",");
							type.add(input.substring(begin+3, end));
						}
					}
					br.close();
		    	} 
				catch (IOException e) {
					e.printStackTrace();
				}		
			}

		}
		catch (MalformedURLException e) {
			e.printStackTrace();
	    } 
	    catch (IOException e) {
	    	e.printStackTrace();
	    }
		Pokemon pokemonAnalisado = new Pokemon(nome, abilities, height, weight, type);
		setPokemon(pokemonAnalisado);
	}
}
