package model;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.swing.JOptionPane;

public class Treinador {
	private String nome;
	private String login;
	private String senha;
	private Vector<Pokemon> pokemons;

	public Treinador(String nome, String login, String senha) {
		setNome(nome);
		setLogin(login);
		setSenha(senha);
		pokemons = new Vector<Pokemon>();
	}
	public Treinador(String nome, String login, String senha, Vector<Pokemon> pokemons) {
		setNome(nome);
		setLogin(login);
		setSenha(senha);
		setPokemons(pokemons);
	}
		
	public String getNome() {
		return nome;
	}
	private void setNome(String nome) {
		this.nome = nome;
	}
	public String getLogin() {
		return login;
	}
	private void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	private void setSenha(String senha) {
		this.senha = senha;
	}
	public Vector<Pokemon> getPokemons() {
		return pokemons;
	}
	private void setPokemons(Vector<Pokemon> pokemons) {
		this.pokemons = pokemons;
	}
	
	public void visualizarPokemons() {
		if(pokemons.size()>0) {
			System.out.println("Pokémon(s)");
			for(Pokemon p: pokemons) {
				System.out.println("     Nome: " + p.getNome());
			}
		}
		else {
			System.out.println("Você não possui Pokémons :(");
		}
	}
	
	public void imprimeDados() {
		System.out.println("Login: " + getLogin());
		System.out.println("Nome: " + getNome());
		System.out.println();
	}
	
	public void add(Pokemon pokemon) {
		if(pokemon.getNome().intern()!="") {
			JOptionPane.showMessageDialog(null, "Pokémon capturado", "Captura", JOptionPane.PLAIN_MESSAGE);
			pokemons.add(pokemon);
		}
	}
	
	public void remove(Pokemon pokemon) {
		if(pokemon.getNome().intern()!="") {
			int size_pokemons = pokemons.size();
			boolean possuiOPokemon = false;
			for(int i=0; i<size_pokemons; ++i) {
				if(pokemons.get(i).getNome().intern()==pokemon.getNome().intern()) {
					pokemons.remove(i);
					possuiOPokemon = true;
					break;
				}
			}
			if(possuiOPokemon) {
				JOptionPane.showMessageDialog(null, "Pokémon libertado", "Libertar", JOptionPane.PLAIN_MESSAGE);
			}
			else {
				JOptionPane.showMessageDialog(null, "Você não possui esse pokémon", "Libertar", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	public void salvar() {
		FileWriter arq;
		try {
			arq = new FileWriter("data/" + getLogin() + ".txt");
			PrintWriter gravarArq = new PrintWriter(arq);
			gravarArq.println(getLogin());
			gravarArq.println(getSenha());
			gravarArq.println(getNome());
			for(Pokemon p: pokemons) {
				gravarArq.println(p.getNome());
			}
			
			arq.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
