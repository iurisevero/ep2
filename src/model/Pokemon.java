package model;

import java.util.Vector;

public class Pokemon {
		private Integer height;
		private Integer weight;
		private String nome;
		private Vector<String> abilities;
		private Vector<String> type;
		
		public Pokemon() {
			nome = "";
		}
		
		public Pokemon(String nome, Vector<String> abilities, Integer height, Integer weight, Vector<String> type) {
			setNome(nome);
			setAbilities(abilities);
			setHeight(height);
			setWeight(weight);
			setType(type);
		}
		
		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}

		public Vector<String> getAbilities() {
			return abilities;
		}

		public void setAbilities(Vector<String> abilities) {
			this.abilities = abilities;
		}

		public Integer getHeight() {
			return height;
		}

		public void setHeight(Integer height) {
			this.height = height;
		}

		public Integer getWeight() {
			return weight;
		}

		public void setWeight(Integer weight) {
			this.weight = weight;
		}

		public Vector<String> getType() {
			return type;
		}

		public void setType(Vector<String> type) {
			this.type = type;
		}		
}
